/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.dto;

import java.util.Date;
import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public class Film {
//    Attributes:
//
//title string -- The title of this film
//episode_id integer -- The episode number of this film.
//opening_crawl string -- The opening paragraphs at the beginning of this film.
//director string -- The name of the director of this film.
//producer string -- The name(s) of the producer(s) of this film. Comma separated.
//release_date date -- The ISO 8601 date format of film release at original creator country.
//species array -- An array of species resource URLs that are in this film.
//starships array -- An array of starship resource URLs that are in this film.
//vehicles array -- An array of vehicle resource URLs that are in this film.
//characters array -- An array of people resource URLs that are in this film.
//planets array -- An array of planet resource URLs that are in this film.
//url string -- the hypermedia URL of this resource.
//created string -- the ISO 8601 date format of the time that this resource was created.
//edited string -- the ISO 8601 date format of the time that this resource was edited.

    private int id;
    private String title;
    private int episode_id;
    private String opening_crawl;
    private String director;
    private String producer;
    private Date release_date;
    private List<Species> species;
    private List<Starship> starships;
    private List<Vehicle> vehicles;
    private List<Person> characters;
    private List<Planet> planets;
    private String url;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the episode_id
     */
    public int getEpisode_id() {
        return episode_id;
    }

    /**
     * @param episode_id the episode_id to set
     */
    public void setEpisode_id(int episode_id) {
        this.episode_id = episode_id;
    }

    /**
     * @return the opening_crawl
     */
    public String getOpening_crawl() {
        return opening_crawl;
    }

    /**
     * @param opening_crawl the opening_crawl to set
     */
    public void setOpening_crawl(String opening_crawl) {
        this.opening_crawl = opening_crawl;
    }

    /**
     * @return the director
     */
    public String getDirector() {
        return director;
    }

    /**
     * @param director the director to set
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     * @return the producer
     */
    public String getProducer() {
        return producer;
    }

    /**
     * @param producer the producer to set
     */
    public void setProducer(String producer) {
        this.producer = producer;
    }

    /**
     * @return the release_date
     */
    public Date getRelease_date() {
        return release_date;
    }

    /**
     * @param release_date the release_date to set
     */
    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    /**
     * @return the species
     */
    public List<Species> getSpecies() {
        return species;
    }

    /**
     * @param species the species to set
     */
    public void setSpecies(List<Species> species) {
        this.species = species;
    }

    /**
     * @return the starships
     */
    public List<Starship> getStarships() {
        return starships;
    }

    /**
     * @param starships the starships to set
     */
    public void setStarships(List<Starship> starships) {
        this.starships = starships;
    }

    /**
     * @return the vehicles
     */
    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    /**
     * @param vehicles the vehicles to set
     */
    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    /**
     * @return the characters
     */
    public List<Person> getCharacters() {
        return characters;
    }

    /**
     * @param characters the characters to set
     */
    public void setCharacters(List<Person> characters) {
        this.characters = characters;
    }

    /**
     * @return the planets
     */
    public List<Planet> getPlanets() {
        return planets;
    }

    /**
     * @param planets the planets to set
     */
    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
}
