/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.dto;

import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public class Starship {
//    Attributes:
//
//name string -- The name of this starship. The common name, such as "Death Star".
//model string -- The model or official name of this starship. Such as "T-65 X-wing" or "DS-1 Orbital Battle Station".
//starship_class string -- The class of this starship, such as "Starfighter" or "Deep Space Mobile Battlestation"
//manufacturer string -- The manufacturer of this starship. Comma separated if more than one.
//cost_in_credits string -- The cost of this starship new, in galactic credits.
//length string -- The length of this starship in meters.
//crew string -- The number of personnel needed to run or pilot this starship.
//passengers string -- The number of non-essential people this starship can transport.
//max_atmosphering_speed string -- The maximum speed of this starship in the atmosphere. "N/A" if this starship is incapable of atmospheric flight.
//hyperdrive_rating string -- The class of this starships hyperdrive.
//MGLT string -- The Maximum number of Megalights this starship can travel in a standard hour. A "Megalight" is a standard unit of distance and has never been defined before within the Star Wars universe. This figure is only really useful for measuring the difference in speed of starships. We can assume it is similar to AU, the distance between our Sun (Sol) and Earth.
//cargo_capacity string -- The maximum number of kilograms that this starship can transport.
//consumables *string
//The maximum length of time that this starship can provide consumables for its entire crew without having to resupply.
//films array -- An array of Film URL Resources that this starship has appeared in.
//pilots array -- An array of People URL Resources that this starship has been piloted by.
//url string -- the hypermedia URL of this resource.
//created string -- the ISO 8601 date format of the time that this resource was created.
//edited string -- the ISO 8601 date format of the time that this resource was edited.

    private int id;
    private String name;
    private String model;
    private String starship_class;
    private String manufacturer;
    private String cost_in_credits;
    private String length;
    private String crew;
    private String passengers;
    private String max_atmosphere_speed;
    private String hyperdrive_rating;
    private String MGLT;
    private String cargo_capacity;
    private String consumables;
    private List<Film> films;
    private List<Person> pilots;
    private String url;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the starship_class
     */
    public String getStarship_class() {
        return starship_class;
    }

    /**
     * @param starship_class the starship_class to set
     */
    public void setStarship_class(String starship_class) {
        this.starship_class = starship_class;
    }

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * @param manufacturer the manufacturer to set
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * @return the cost_in_credits
     */
    public String getCost_in_credits() {
        return cost_in_credits;
    }

    /**
     * @param cost_in_credits the cost_in_credits to set
     */
    public void setCost_in_credits(String cost_in_credits) {
        this.cost_in_credits = cost_in_credits;
    }

    /**
     * @return the length
     */
    public String getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(String length) {
        this.length = length;
    }

    /**
     * @return the crew
     */
    public String getCrew() {
        return crew;
    }

    /**
     * @param crew the crew to set
     */
    public void setCrew(String crew) {
        this.crew = crew;
    }

    /**
     * @return the passengers
     */
    public String getPassengers() {
        return passengers;
    }

    /**
     * @param passengers the passengers to set
     */
    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    /**
     * @return the max_atmosphere_speed
     */
    public String getMax_atmosphere_speed() {
        return max_atmosphere_speed;
    }

    /**
     * @param max_atmosphere_speed the max_atmosphere_speed to set
     */
    public void setMax_atmosphere_speed(String max_atmosphere_speed) {
        this.max_atmosphere_speed = max_atmosphere_speed;
    }

    /**
     * @return the hyperdrive_rating
     */
    public String getHyperdrive_rating() {
        return hyperdrive_rating;
    }

    /**
     * @param hyperdrive_rating the hyperdrive_rating to set
     */
    public void setHyperdrive_rating(String hyperdrive_rating) {
        this.hyperdrive_rating = hyperdrive_rating;
    }

    /**
     * @return the MGLT
     */
    public String getMGLT() {
        return MGLT;
    }

    /**
     * @param MGLT the MGLT to set
     */
    public void setMGLT(String MGLT) {
        this.MGLT = MGLT;
    }

    /**
     * @return the cargo_capacity
     */
    public String getCargo_capacity() {
        return cargo_capacity;
    }

    /**
     * @param cargo_capacity the cargo_capacity to set
     */
    public void setCargo_capacity(String cargo_capacity) {
        this.cargo_capacity = cargo_capacity;
    }

    /**
     * @return the consumables
     */
    public String getConsumables() {
        return consumables;
    }

    /**
     * @param consumables the consumables to set
     */
    public void setConsumables(String consumables) {
        this.consumables = consumables;
    }

    /**
     * @return the films
     */
    public List<Film> getFilms() {
        return films;
    }

    /**
     * @param films the films to set
     */
    public void setFilms(List<Film> films) {
        this.films = films;
    }

    /**
     * @return the pilots
     */
    public List<Person> getPilots() {
        return pilots;
    }

    /**
     * @param pilots the pilots to set
     */
    public void setPilots(List<Person> pilots) {
        this.pilots = pilots;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
}
