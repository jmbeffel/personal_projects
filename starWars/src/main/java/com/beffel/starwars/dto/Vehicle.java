/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.dto;

import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public class Vehicle {
//    Attributes:
//
//name string -- The name of this vehicle. The common name, such as "Sand Crawler" or "Speeder bike".
//model string -- The model or official name of this vehicle. Such as "All-Terrain Attack Transport".
//vehicle_class string -- The class of this vehicle, such as "Wheeled" or "Repulsorcraft".
//manufacturer string -- The manufacturer of this vehicle. Comma separated if more than one.
//length string -- The length of this vehicle in meters.
//cost_in_credits string -- The cost of this vehicle new, in Galactic Credits.
//crew string -- The number of personnel needed to run or pilot this vehicle.
//passengers string -- The number of non-essential people this vehicle can transport.
//max_atmosphering_speed string -- The maximum speed of this vehicle in the atmosphere.
//cargo_capacity string -- The maximum number of kilograms that this vehicle can transport.
//consumables *string
//The maximum length of time that this vehicle can provide consumables for its entire crew without having to resupply.
//films array -- An array of Film URL Resources that this vehicle has appeared in.
//pilots array -- An array of People URL Resources that this vehicle has been piloted by.
//url string -- the hypermedia URL of this resource.
//created string -- the ISO 8601 date format of the time that this resource was created.
//edited string -- the ISO 8601 date format of the time that this resource was edited.
    
    int id;
    private String name;
    private String model;
    private String vehicle_class;
    private String length;
    private String cost_in_credits;
    private String crew;
    private String passengers;
    private String max_atmosphere_speed;
    private String cargo_capacity;
    private String consumables;
    private List<Film> films;
    private List<Person> pilots;
    private String url;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the vehicle_class
     */
    public String getVehicle_class() {
        return vehicle_class;
    }

    /**
     * @param vehicle_class the vehicle_class to set
     */
    public void setVehicle_class(String vehicle_class) {
        this.vehicle_class = vehicle_class;
    }

    /**
     * @return the length
     */
    public String getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(String length) {
        this.length = length;
    }

    /**
     * @return the cost_in_credits
     */
    public String getCost_in_credits() {
        return cost_in_credits;
    }

    /**
     * @param cost_in_credits the cost_in_credits to set
     */
    public void setCost_in_credits(String cost_in_credits) {
        this.cost_in_credits = cost_in_credits;
    }

    /**
     * @return the crew
     */
    public String getCrew() {
        return crew;
    }

    /**
     * @param crew the crew to set
     */
    public void setCrew(String crew) {
        this.crew = crew;
    }

    /**
     * @return the passengers
     */
    public String getPassengers() {
        return passengers;
    }

    /**
     * @param passengers the passengers to set
     */
    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    /**
     * @return the max_atmosphere_speed
     */
    public String getMax_atmosphere_speed() {
        return max_atmosphere_speed;
    }

    /**
     * @param max_atmosphere_speed the max_atmosphere_speed to set
     */
    public void setMax_atmosphere_speed(String max_atmosphere_speed) {
        this.max_atmosphere_speed = max_atmosphere_speed;
    }

    /**
     * @return the cargo_capacity
     */
    public String getCargo_capacity() {
        return cargo_capacity;
    }

    /**
     * @param cargo_capacity the cargo_capacity to set
     */
    public void setCargo_capacity(String cargo_capacity) {
        this.cargo_capacity = cargo_capacity;
    }

    /**
     * @return the consumables
     */
    public String getConsumables() {
        return consumables;
    }

    /**
     * @param consumables the consumables to set
     */
    public void setConsumables(String consumables) {
        this.consumables = consumables;
    }

    /**
     * @return the films
     */
    public List<Film> getFilms() {
        return films;
    }

    /**
     * @param films the films to set
     */
    public void setFilms(List<Film> films) {
        this.films = films;
    }

    /**
     * @return the pilots
     */
    public List<Person> getPilots() {
        return pilots;
    }

    /**
     * @param pilots the pilots to set
     */
    public void setPilots(List<Person> pilots) {
        this.pilots = pilots;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
}
