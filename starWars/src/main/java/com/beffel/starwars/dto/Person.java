/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.dto;

import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public class Person {
//    Attributes:
//
//name string -- The name of this person.
//birth_year string -- The birth year of the person, using the in-universe standard of BBY or ABY - Before the Battle of Yavin or After the Battle of Yavin. The Battle of Yavin is a battle that occurs at the end of Star Wars episode IV: A New Hope.
//eye_color string -- The eye color of this person. Will be "unknown" if not known or "n/a" if the person does not have an eye.
//gender string -- The gender of this person. Either "Male", "Female" or "unknown", "n/a" if the person does not have a gender.
//hair_color string -- The hair color of this person. Will be "unknown" if not known or "n/a" if the person does not have hair.
//height string -- The height of the person in centimeters.
//mass string -- The mass of the person in kilograms.
//skin_color string -- The skin color of this person.
//homeworld string -- The URL of a planet resource, a planet that this person was born on or inhabits.
//films array -- An array of film resource URLs that this person has been in.
//species array -- An array of species resource URLs that this person belongs to.
//starships array -- An array of starship resource URLs that this person has piloted.
//vehicles array -- An array of vehicle resource URLs that this person has piloted.
//url string -- the hypermedia URL of this resource.
//created string -- the ISO 8601 date format of the time that this resource was created.
//edited string -- the ISO 8601 date format of the time that this resource was edited.

    private int id;
    private String name;
    private String birth_year;
    private String eye_color;
    private String gender;
    private String hair_color;
    private String height;
    private String mass;
    private String skin_color;
    private String homeworld;
    private List<Film> films;
    private List<Species> species;
    private List<Starship> starships;
    private List<Vehicle> vehicles;
    private String url;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the birth_year
     */
    public String getBirth_year() {
        return birth_year;
    }

    /**
     * @param birth_year the birth_year to set
     */
    public void setBirth_year(String birth_year) {
        this.birth_year = birth_year;
    }

    /**
     * @return the eye_color
     */
    public String getEye_color() {
        return eye_color;
    }

    /**
     * @param eye_color the eye_color to set
     */
    public void setEye_color(String eye_color) {
        this.eye_color = eye_color;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the hair_color
     */
    public String getHair_color() {
        return hair_color;
    }

    /**
     * @param hair_color the hair_color to set
     */
    public void setHair_color(String hair_color) {
        this.hair_color = hair_color;
    }

    /**
     * @return the height
     */
    public String getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * @return the mass
     */
    public String getMass() {
        return mass;
    }

    /**
     * @param mass the mass to set
     */
    public void setMass(String mass) {
        this.mass = mass;
    }

    /**
     * @return the skin_color
     */
    public String getSkin_color() {
        return skin_color;
    }

    /**
     * @param skin_color the skin_color to set
     */
    public void setSkin_color(String skin_color) {
        this.skin_color = skin_color;
    }

    /**
     * @return the homeworld
     */
    public String getHomeworld() {
        return homeworld;
    }

    /**
     * @param homeworld the homeworld to set
     */
    public void setHomeworld(String homeworld) {
        this.homeworld = homeworld;
    }

    /**
     * @return the films
     */
    public List<Film> getFilms() {
        return films;
    }

    /**
     * @param films the films to set
     */
    public void setFilms(List<Film> films) {
        this.films = films;
    }

    /**
     * @return the species
     */
    public List<Species> getSpecies() {
        return species;
    }

    /**
     * @param species the species to set
     */
    public void setSpecies(List<Species> species) {
        this.species = species;
    }

    /**
     * @return the starships
     */
    public List<Starship> getStarships() {
        return starships;
    }

    /**
     * @param starships the starships to set
     */
    public void setStarships(List<Starship> starships) {
        this.starships = starships;
    }

    /**
     * @return the vehicles
     */
    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    /**
     * @param vehicles the vehicles to set
     */
    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
}
