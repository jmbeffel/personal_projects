/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.dto;

import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public class Planet {
//    Attributes:
//
//name string -- The name of this planet.
//diameter string -- The diameter of this planet in kilometers.
//rotation_period string -- The number of standard hours it takes for this planet to complete a single rotation on its axis.
//orbital_period string -- The number of standard days it takes for this planet to complete a single orbit of its local star.
//gravity string -- A number denoting the gravity of this planet, where "1" is normal or 1 standard G. "2" is twice or 2 standard Gs. "0.5" is half or 0.5 standard Gs.
//population string -- The average population of sentient beings inhabiting this planet.
//climate string -- The climate of this planet. Comma separated if diverse.
//terrain string -- The terrain of this planet. Comma separated if diverse.
//surface_water string -- The percentage of the planet surface that is naturally occurring water or bodies of water.
//residents array -- An array of People URL Resources that live on this planet.
//films array -- An array of Film URL Resources that this planet has appeared in.
//url string -- the hypermedia URL of this resource.
//created string -- the ISO 8601 date format of the time that this resource was created.
//edited string -- the ISO 8601 date format of the time that this resource was edited.
    
    private int id;
    private String name;
    private String diameter;
    private String rotation_period;
    private String orbital_period;
    private String gravity;
    private String population;
    private String climate;
    private String terrain;
    private String surface_water;
    private List<Person> residents;
    private List<Film> films;
    private String url;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the diameter
     */
    public String getDiameter() {
        return diameter;
    }

    /**
     * @param diameter the diameter to set
     */
    public void setDiameter(String diameter) {
        this.diameter = diameter;
    }

    /**
     * @return the rotation_period
     */
    public String getRotation_period() {
        return rotation_period;
    }

    /**
     * @param rotation_period the rotation_period to set
     */
    public void setRotation_period(String rotation_period) {
        this.rotation_period = rotation_period;
    }

    /**
     * @return the orbital_period
     */
    public String getOrbital_period() {
        return orbital_period;
    }

    /**
     * @param orbital_period the orbital_period to set
     */
    public void setOrbital_period(String orbital_period) {
        this.orbital_period = orbital_period;
    }

    /**
     * @return the gravity
     */
    public String getGravity() {
        return gravity;
    }

    /**
     * @param gravity the gravity to set
     */
    public void setGravity(String gravity) {
        this.gravity = gravity;
    }

    /**
     * @return the population
     */
    public String getPopulation() {
        return population;
    }

    /**
     * @param population the population to set
     */
    public void setPopulation(String population) {
        this.population = population;
    }

    /**
     * @return the climate
     */
    public String getClimate() {
        return climate;
    }

    /**
     * @param climate the climate to set
     */
    public void setClimate(String climate) {
        this.climate = climate;
    }

    /**
     * @return the terrain
     */
    public String getTerrain() {
        return terrain;
    }

    /**
     * @param terrain the terrain to set
     */
    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    /**
     * @return the surface_water
     */
    public String getSurface_water() {
        return surface_water;
    }

    /**
     * @param surface_water the surface_water to set
     */
    public void setSurface_water(String surface_water) {
        this.surface_water = surface_water;
    }

    /**
     * @return the residents
     */
    public List<Person> getResidents() {
        return residents;
    }

    /**
     * @param residents the residents to set
     */
    public void setResidents(List<Person> residents) {
        this.residents = residents;
    }

    /**
     * @return the films
     */
    public List<Film> getFilms() {
        return films;
    }

    /**
     * @param films the films to set
     */
    public void setFilms(List<Film> films) {
        this.films = films;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
}
