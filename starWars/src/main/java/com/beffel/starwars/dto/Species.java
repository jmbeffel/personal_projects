/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.dto;

/**
 *
 * @author jasonbeffel
 */
public class Species {
//    Attributes:
//
//name string -- The name of this species.
//classification string -- The classification of this species, such as "mammal" or "reptile".
//designation string -- The designation of this species, such as "sentient".
//average_height string -- The average height of this species in centimeters.
//average_lifespan string -- The average lifespan of this species in years.
//eye_colors string -- A comma-separated string of common eye colors for this species, "none" if this species does not typically have eyes.
//hair_colors string -- A comma-separated string of common hair colors for this species, "none" if this species does not typically have hair.
//skin_colors string -- A comma-separated string of common skin colors for this species, "none" if this species does not typically have skin.
//language string -- The language commonly spoken by this species.
//homeworld string -- The URL of a planet resource, a planet that this species originates from.
//people array -- An array of People URL Resources that are a part of this species.
//films array -- An array of Film URL Resources that this species has appeared in.
//url string -- the hypermedia URL of this resource.
//created string -- the ISO 8601 date format of the time that this resource was created.
//edited string -- the ISO 8601 date format of the time that this resource was edited.
    
    private int id;
    private String name;
    private String classification;
    private String designation;
    private String average_height;
    private String average_lifespan;
    private String eye_colors;
    private String hair_colors;
    private String skin_colors;
    private String language;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the classification
     */
    public String getClassification() {
        return classification;
    }

    /**
     * @param classification the classification to set
     */
    public void setClassification(String classification) {
        this.classification = classification;
    }

    /**
     * @return the designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * @param designation the designation to set
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * @return the average_height
     */
    public String getAverage_height() {
        return average_height;
    }

    /**
     * @param average_height the average_height to set
     */
    public void setAverage_height(String average_height) {
        this.average_height = average_height;
    }

    /**
     * @return the average_lifespan
     */
    public String getAverage_lifespan() {
        return average_lifespan;
    }

    /**
     * @param average_lifespan the average_lifespan to set
     */
    public void setAverage_lifespan(String average_lifespan) {
        this.average_lifespan = average_lifespan;
    }

    /**
     * @return the eye_colors
     */
    public String getEye_colors() {
        return eye_colors;
    }

    /**
     * @param eye_colors the eye_colors to set
     */
    public void setEye_colors(String eye_colors) {
        this.eye_colors = eye_colors;
    }

    /**
     * @return the hair_colors
     */
    public String getHair_colors() {
        return hair_colors;
    }

    /**
     * @param hair_colors the hair_colors to set
     */
    public void setHair_colors(String hair_colors) {
        this.hair_colors = hair_colors;
    }

    /**
     * @return the skin_colors
     */
    public String getSkin_colors() {
        return skin_colors;
    }

    /**
     * @param skin_colors the skin_colors to set
     */
    public void setSkin_colors(String skin_colors) {
        this.skin_colors = skin_colors;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }
    
    
}
