/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.controller;

import com.beffel.starwars.dao.PersonDao;
import com.beffel.starwars.dto.Person;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author jasonbeffel
 */
@Controller
public class PersonController {
    
    private PersonDao personDao;
    
    @Inject
    public PersonController(PersonDao personDao){
        this.personDao = personDao;
    }
    
    @RequestMapping(value = "/people", method = RequestMethod.GET)
    public String goToPeople(Map<String, Object> model) {
        model.put("message", "The People of Star Wars" );
        return "people";
    }
    
    final String swapiUrl = "http://swapi.co/api";

//    @CrossOrigin(origins = swapiUrl)
//    @RequestMapping(value="/people", method=RequestMethod.GET)
//    public List<Person> getAllPeople(){
//        return personDao.getAllPeople();
//    }
    
//    @CrossOrigin(origins = swapiUrl)
//    @RequestMapping(value="/people/{id}", method=RequestMethod.GET)
//    @ResponseBody
//    public Person getPerson(@PathVariable int id){
//        return personDao.getPersonById(id);
//    }
    
    /*
    @CrossOrigin(origins = "http://localhost:9000")
    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(required=false, defaultValue="World") String name) {
        System.out.println("==== in greeting ====");
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }
    */
    
}
