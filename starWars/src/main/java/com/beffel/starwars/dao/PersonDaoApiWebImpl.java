/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.dao;

import com.beffel.starwars.dto.Person;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jasonbeffel
 */
public class PersonDaoApiWebImpl implements PersonDao{

    private static Map<Integer, Person> people = new HashMap();
    
//    public PersonDaoApiWebImpl(){
////        for(int i=1;i<=count;i++){
////            addPersonToPeople(i);
////        }
//    }
    
    
    @Override
    public List<Person> getAllPeople() {
        return (List<Person>) people.values();
    }

    @Override
    public Person getPersonById(int id) {
        return people.get(id);
    }

    @Override
    public void addPersonToPeople(int id) {
        Person person = new Person();
        person.setId(id);
        people.put(person.getId(),person);
    }

    @Override
    public void updatePerson(Person person) {
        people.put(person.getId(), person);
    }
    
}
