/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.starwars.dao;

import com.beffel.starwars.dto.Person;
import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public interface PersonDao {
    
    public void addPersonToPeople(int id);
    public List<Person> getAllPeople();
    public Person getPersonById(int id);
    public void updatePerson(Person person);
    
}
