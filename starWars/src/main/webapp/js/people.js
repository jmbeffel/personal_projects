/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//when the document is ready, load the information
$(document).ready(function () {
    var swapiPeopleUrl = 'http://swapi.co/api/people/';
    loadTable(swapiPeopleUrl);
    createPersonDisplayTable();
});

function loadTable(url) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (json, status) {
            clearPeopleTable();
            fillPeopleTable(json, status);
        }
    });
}

function fillPeopleTable(jsonData, status) {
    var json = jsonData.results;
    var peopleTableSummary = $('#peopleRows');
//    var next = jsonData.next;
    $.each(json, function (arrayPosition, person) {
//        $.ajax({
//            url: person.species,
//            success: function (data) {
//                $("#peopleSpecies"+arrayPosition).html(data.name);
//            }
//        });
        peopleTableSummary.append($('<tr>')
                .append($('<td>').attr({'onClick': 'displayPersonInfo("' + person.url + '")'}).html(person.name))
                .append($('<td id="peopleSpecies' + arrayPosition + '">').html(person.height))
                .append($('<td>').html(person.mass))
                .append($('<td>').html(person.gender)));
    });
}

function clearPeopleTable() {
    $('#peopleRows').empty();
}

function displayPersonInfo(personUrl) {
    clearPersonDisplay();
    $.ajax({
        url: personUrl,
        dataType: 'json'
    }).success(function (data, status) {
        fillPersonDisplay(data, status);
    });

}

function clearPersonDisplay() {
    $('#people-person-details-name').empty();
    $('#people-person-details-details').empty();
    $('#people-person-details-eyecolor').empty();
    $('#people-person-details-gender').empty();
    $('#people-person-details-haircolor').empty();
    $('#people-person-details-height').empty();
    $('#people-person-details-mass').empty();
    $('#people-person-details-skincolor').empty();
    $('#people-person-details-homeworld').empty();
}

function fillPersonDisplay(personJson, status) {
    clearPersonDisplay();
    var personDisplayDiv = $('#people-person-details');
    personDisplayDiv.show();
    $('#people-person-details-name').append(personJson.name);
    $('#people-person-details-eyecolor').append(personJson.eye_color);
    $('#people-person-details-gender').append(personJson.gender);
    $('#people-person-details-haircolor').append(personJson.hair_color);
    $('#people-person-details-height').append(personJson.height+' cm');
    $('#people-person-details-mass').append(personJson.mass+' kg');
    $('#people-person-details-skincolor').append(personJson.skin_color);
//    $('#people-person-details-homeworld').append(personJson.homeworld);
    $.ajax({
        url: personJson.homeworld,
        success: function (planetJson){
            $('#people-person-details-homeworld').append(planetJson.name);
        }
    });
}

function createPersonDisplayTable () {
    var personDisplayDiv = $('#people-person-details');
    personDisplayDiv.hide();
    personDisplayDiv.append($('<div class="row">')
            .append($('<div class="col-xs-3">').text('Name:'))
            .append($('<div id="people-person-details-name" class="col-xs-9">')))
    .append($('<div class="row">')
            .append($('<div class="col-xs-3">').text('Eye Color:'))
            .append($('<div id="people-person-details-eyecolor" class="col-xs-9">')))
    .append($('<div class="row">')
            .append($('<div class="col-xs-3">').text('Gender:'))
            .append($('<div id="people-person-details-gender" class="col-xs-9">')))
    .append($('<div class="row">')
            .append($('<div class="col-xs-3">').text('Hair Color:'))
            .append($('<div id="people-person-details-haircolor" class="col-xs-9">')))
    .append($('<div class="row">')
            .append($('<div class="col-xs-3">').text('Height:'))
            .append($('<div id="people-person-details-height" class="col-xs-9">')))
    .append($('<div class="row">')
            .append($('<div class="col-xs-3">').text('Mass:'))
            .append($('<div id="people-person-details-mass" class="col-xs-9">')))
    .append($('<div class="row">')
            .append($('<div class="col-xs-3">').text('Skin Color:'))
            .append($('<div id="people-person-details-skincolor" class="col-xs-9">')))
    .append($('<div class="row">')
            .append($('<div class="col-xs-3">').text('Homeworld:'))
            .append($('<div id="people-person-details-homeworld" class="col-xs-9">')))
    ;
}
