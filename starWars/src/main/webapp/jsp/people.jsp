<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>People</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/starWars/Darth-Vader-icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Jason Beffel's Personal Projects</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/people">People</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/starships">Starships</a></li>
                </ul>    
            </div>
            <h2>${message}</h2>
            <div class="row">
                <div id="people-person-details" class="col-md-6">
                </div>
                <div class="col-md-6">
                    <table id="peopleTable" class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th><h3>Name</h3></th>
                                <th><h3>Height(cm)</h3></th>
                                <th><h3>Mass(kg)</h3></th>
                                <th><h3>Gender</h3></th>
                            </tr>
                        </thead>
                        <tbody id="peopleRows"></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/people.js"></script>

    </body>
</html>