<%-- 
    Document   : singleFileUpload
    Created on : Feb 13, 2017, 3:52:43 PM
    Author     : jasonbeffel
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Upload Single File</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/starWars/Darth-Vader-icon.png">
        <style>

        </style>
    </head>
    <body>
        <!-- The important point to note is that form enctype should be 
        multipart/form-data, so that Spring web application knows that the 
        request contains file data that needs to be processed. -->
        <h1>Upload Single File</h1>
        <form action="fileUpload" method="POST" enctype="multipart/form-data">
            File to upload: <input type="file" name="file"/>
            
            Name: <input type="text" name="name"/>
            
            <input type="submit" value="Upload"/>
            
        </form>
    </body>
</html>
