/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.photouploadpractice.dao;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jasonbeffel
 */
public class PhotoDaoFileImpl implements PhotoDao {

    @Override
    public void save(MultipartFile file, String userFileName) {
        //creating the path to save files
//        StringBuilder sb = new StringBuilder(ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath(""));
        //for the uploads folder
//        sb.append("uploads/");
//        String applicationUploadsPath = sb.toString();
//        if (!new File(applicationUploadsPath).exists()) {
//            new File(applicationUploadsPath).mkdir();
//        }
        
        String fileName = file.getOriginalFilename();
        //adding the file name to the path
//        sb.append(file.getOriginalFilename());
//        sb.append(fileName);
//        String filePath = sb.toString();
//        File destination = new File(filePath);
        File destination = new File("uploads/", fileName);
//        File destination = new File(fileName);
        
        try {
            file.transferTo(destination);
        } catch (IOException ex) {
            Logger.getLogger(PhotoDaoFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalStateException ex) {
            Logger.getLogger(PhotoDaoFileImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Resource loadAsResource(String fileName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
