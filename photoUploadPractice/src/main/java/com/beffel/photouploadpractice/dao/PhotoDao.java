/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.photouploadpractice.dao;

import javax.annotation.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jasonbeffel
 */
public interface PhotoDao {
    
    public void save(MultipartFile file, String fileName);
    public Resource loadAsResource(String fileName);
    
}
