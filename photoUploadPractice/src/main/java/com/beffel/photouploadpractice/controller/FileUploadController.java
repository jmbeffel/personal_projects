/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.photouploadpractice.controller;

import com.beffel.photouploadpractice.dao.PhotoDao;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jasonbeffel
 */

@Controller
public class FileUploadController {
    
    private PhotoDao photoDao;
    
    @Inject
    public FileUploadController(PhotoDao photoDao){
        this.photoDao = photoDao;
    }
    
    @RequestMapping(value="/fileUpload", method=RequestMethod.GET)
    public String displayForm(){
        return "singleFileUpload";
    }
    
    @RequestMapping(value="/fileUpload", method = RequestMethod.POST)
    public String uploadFormFile(@RequestParam(value = "name", required = false) String name, @RequestParam("file") MultipartFile file){
        if (!file.isEmpty()) {
            //byte[] bytes = file.getBytes();
            // store the bytes somewhere
            
            photoDao.save(file, name);
            return "redirect:uploadSuccess";
        }

        return "redirect:uploadFailure";
    }
    
    @RequestMapping(value="uploadSuccess", method = RequestMethod.GET)
    public String displaySuccessPage(){
        return "uploadSuccess";
    }
    
    @RequestMapping(value="uploadFailure", method = RequestMethod.GET)
    public String displayFailurePage(){
        return "uploadFailure";
    }
    
}
