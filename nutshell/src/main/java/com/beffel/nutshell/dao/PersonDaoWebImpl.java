/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.nutshell.dao;

import com.beffel.nutshell.dto.Person;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public class PersonDaoWebImpl implements PersonDao {

    private static ArrayList<Person> peopleList = null;
    
    @Override
    public List<Person> getAllPeople() {
        return peopleList;
    }

    @Override
    public void addPerson(Person person) {
        peopleList.add(person);
    }
    
}
