/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.nutshell.dao;

import com.beffel.nutshell.dto.Person;
import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public interface PersonDao {
    public List<Person> getAllPeople();
    public void addPerson(Person person);
    
}
