drop database if exists treeline_books;
create database treeline_books;
use treeline_books;
create table `Book`(
`bookId` int not null auto_increment,
`title` varchar(50) not null,
`author` varchar(50) not null,
`publisher` varchar(50) not null,
primary key (bookId)
);

insert into `Book` values (1, "Redwall", "Brian Jaques", "Ace Books"),
(2, "Mossflower", "Brian Jaques", "Ace Books"),
(3, "Mattimeo", "Brian Jaques", "Ace Books"),
(4, "Mariel of Redwall", "Brian Jaques", "Ace Books"),
(5, "Salamandastrom", "Brian Jaques", "Ace Books"),
(6, "The Bellmaker", "Brian Jaques", "Ace Books"),
(7, "The Outcast of Redwall", "Brian Jaques", "Ace Books"),
(8, "Martin the Warrior", "Brian Jaques", "Ace Books");