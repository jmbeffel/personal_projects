<%-- 
    Document   : tabs
    Created on : Feb 10, 2017, 5:31:06 PM
    Author     : jasonbeffel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Tabs Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Tabs Page</h1>
            <hr/>
            <div class="navbar">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}/treelineBooks">Home</a></li>
                 <li role="presentation"><a href="${pageContext.request.contextPath}/other">Other</a></li>
                 <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/tabs">Tabs</a></li>
                </ul>    
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
