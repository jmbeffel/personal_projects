<%-- 
    Document   : treelineBooks
    Created on : Feb 10, 2017, 5:28:12 PM
    Author     : jasonbeffel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>BookController Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>List of Books</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"class="active"><a href="${pageContext.request.contextPath}/treelineBooks">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/other">Other</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tabs">Tabs</a></li>
                </ul>    
            </div>
            <div class="container">
                <table id="bookTable" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th><h3>Title</h3></th>
                            <th><h3>Author</h3></th>
                            <th><h3>Publisher</h3></th>
                        </tr>
                    </thead>
                    <tbody id="bookRows"></tbody>
                </table>
            </div>  
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/treelineBooks.js"></script>

    </body>
</html>
