/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//when the document is ready, put the list of books into the table on the page
$(document).ready(function () {
   loadBooks(); 
});
//load the books into the table
function loadBooks(){
    //get the JSON object from the endpoint
    $.ajax({
        type: 'GET',
        url: 'books'
    }).success(function(data, status){
        populateBookTable(data, status);
    });
}

//function to clear the book list table.
function clearBookTable(){
    $('#bookRows').empty();
}

//function to create the rest of the book table and put the books there.
function populateBookTable(bookList, status){
    //clear any previously shown information.
    clearBookTable();
    
    //store the tbody as a variable
    var bookRows = $('#bookRows');
    
    //use a for each to create the html rows for the book information
    $.each(bookList, function (arrayPosition, book){
        bookRows.append($('<tr>')
                .append($('<td>').append(book.title))
                .append($('<td>').append(book.author))
                .append($('<td>').append(book.publisher))
        );
    });
}