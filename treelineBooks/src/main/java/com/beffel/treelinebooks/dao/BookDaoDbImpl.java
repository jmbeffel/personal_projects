/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.treelinebooks.dao;

import com.beffel.treelinebooks.dto.Book;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author jasonbeffel
 */
public class BookDaoDbImpl implements BookDao {

    //make the jdbcTemplate to be used.
    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    
    //the SQL statements to be used by the jdbc template to get the data from mySQL
    private static final String INSERT = "insert into Book (title, author, publisher) values (?, ?, ?)";
    private static final String DELETE = "delete from Book where bookId = ?";
    private static final String UPDATE = "update Book set title = ?, author = ?, publisher = ? where bookId = ?";
    private static final String SELECT = "select from Dvd where bookId = ?";
    //if I realize I need a select all later... select all from large tables can eventually become really time consuming.
    private static final String SELECT_ALL = "select * from Book";

    @Override
    public void createBook(Book book) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Book getBookById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Book> getAllBooks() {
        return new ArrayList<> (jdbcTemplate.query(SELECT_ALL, new BookMapper()));
    }

    @Override
    public void updateBook(Book book) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteBook(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //the row mapper turns the SQL table row into a Book object.
    private static final class BookMapper implements RowMapper<Book> {
        
        @Override
        public Book mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
            Book book = new Book();
            book.setId(resultSet.getInt("bookId"));
            book.setTitle(resultSet.getString("title"));
            book.setAuthor(resultSet.getString("author"));
            book.setPublisher(resultSet.getString("publisher"));
            return book;
        }
        
    }
    
}
