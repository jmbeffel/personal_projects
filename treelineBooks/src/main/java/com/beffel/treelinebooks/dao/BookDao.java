/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.treelinebooks.dao;

import com.beffel.treelinebooks.dto.Book;
import java.util.List;

/**
 *
 * @author jasonbeffel
 */
public interface BookDao {
    //interface for CRUD
    public void createBook(Book book);
    public Book getBookById(int id);
    public List<Book> getAllBooks();
    public void updateBook(Book book);
    public void deleteBook(int id);
}
