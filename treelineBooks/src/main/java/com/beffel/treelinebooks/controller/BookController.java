/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.treelinebooks.controller;

import com.beffel.treelinebooks.dao.BookDao;
import com.beffel.treelinebooks.dto.Book;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author jasonbeffel
 */
@Controller
public class BookController {
    
    //setting up injection
    private BookDao bookDao;
    
    //injecting the book dao interface into the controller instance.
    @Inject
    public BookController(BookDao bookDao){
        this.bookDao = bookDao;
    }
    
    //need to show the home page
    @RequestMapping(value = {"/", "/treelineBooks"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "treelineBooks";
    }
    
    @RequestMapping(value = "/other", method = RequestMethod.GET)
    public String displayOtherPage() {
        return "other";
    }
    
    @RequestMapping(value = "/tabs", method = RequestMethod.GET)
    public String displayTabsPage() {
        return "tabs";
    }
    
    
    //method to get all the books from the DAO and send it to the front end as a JSON.
    //request mapping - what the front calls to get the books
    //response body - list of books in JSON
    @RequestMapping(value = "/books", method = RequestMethod.GET)
    @ResponseBody public List<Book> getAllBooks(){
        return bookDao.getAllBooks();
    }
    
}
