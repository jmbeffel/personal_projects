/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beffel.treelinebooks;

import com.beffel.treelinebooks.dao.BookDao;
import com.beffel.treelinebooks.dto.Book;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author jasonbeffel
 */
public class TreelineBooksTests {
    
    private BookDao dao;
    
    public TreelineBooksTests() {
    }
    
    
    @Before
    public void setUp() {
        // Ask Spring for my DAO through dependency injection via the test-applicationContext.xml file.
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = (BookDao) ctx.getBean("bookDaoDbImpl");
        //grab the jdbc template to use
        JdbcTemplate jdbcTemplate = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        //this runs the SQL script file every time before each test runs
        JdbcTestUtils.executeSqlScript(jdbcTemplate, new ClassPathResource("/create_book_database_test.sql"), true);
    }
    
    //incase I needed to do some cleanup after each test, however, at this point cleanup and setup are bundled into one step.
    @After
    public void tearDown() {
    }

   
    //all test methods would be annotated
     @Test
     public void testGetAllBooks() {
     //arrange
     List <Book> books = new ArrayList<>();
     //act
     books = dao.getAllBooks();
     //assert
     assertTrue("The loaded book database is not empty", !books.isEmpty());
     assertTrue("The loaded book database doesn't have 8 items.", books.size()==8);
     }
}
